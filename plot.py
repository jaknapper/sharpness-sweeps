import matplotlib.pyplot as plt
import json

# Opening JSON, loading as string and converting to dict, then plotting to test
f = open('data_nb.json')
 
# returns JSON object as a dictionary
data = json.loads(json.load(f))

for key in data['discrete'].keys():
    plt.plot(list(map(float, data['continuous']['down'].keys())),data['continuous']['down'].values(), color = 'grey', label = 'Continuous down')
    plt.plot(list(map(float, data['continuous']['up'].keys())),data['continuous']['up'].values(), color = 'lightgrey', label = 'Continuous up')

    plt.plot(list(map(float, data['discrete'][key]['down'].keys())),data['discrete'][key]['down'].values(), marker = '>', label = 'Discrete down')
    plt.plot(list(map(float, data['discrete'][key]['up'].keys())),data['discrete'][key]['up'].values(), marker = '<', label = 'Discrete up')
    plt.xlabel('Height (steps) \n Markers are to show direction, not every point')
    plt.ylabel('Size (steps)')
    plt.legend()
    plt.tight_layout()
    # plt.show()
    plt.savefig('{}.png'.format(key))
    plt.clf()